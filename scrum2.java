package com.company;

import java.util.Scanner;

public class scrum2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String matriu[][] =
                {{"Dia", "Hora inici", "Hora fi", "Nom", "Organitzador", "Lloc", "Categoria"},
                        {"Dilluns", "9:00", "13:00", "Exposició treballs manuals", "Casal Fent Gran Taulat - Can Saladrigas", "Casal de Gent Gran Taulat Can Saladrigas, c. Llull 214-216", "Exposicio"},
                        {"Dimarts", "17:00", "19:00", "Parxís Infantil", "Comissió de Festes Diagonal-Montseny", "a la pl. Montseny (av. Diagonal 62-82)", "Concurs"},
                        {"Dilluns", "17:00", "20:00", "Dòmino 1a eliminatòria", "Comissió de Festes Diagonal-Montseny", "a la pl. Montseny (av. Diagonal 62-82)", "Concurs"},
                        {"Dimecres", "17:30", "19:30", "PETITS MAKERS: Tecnologies creatives", "Eixos Creativa", "a l'Espai Jove 'Roc Boronat', c. Roc Boronat amb c. Pallars", "Infantil"},
                        {"Dijous", "18:00", "01:00o", "Sopar de veïnes", "Ateneu Popular la Flor de Maig", "a l'Ateneu Popular La Flor de Maig, c. Doctor Trueta, 195", "Trobada"}};

        System.out.println("Ingrese su dirección de correo electronico");
        String correo = sc.nextLine();
            System.out.println("\n" + "Bienvenid@ " +correo+ "\n");
            System.out.println("Festa Major Del Poblenou!!" + "\n");

            System.out.println("Que informacion te interesa? Puedes escoger entre:");
            System.out.println("Actividades");
            System.out.println("Fechas");
            System.out.println("Localizacion");
            System.out.println("Horario general");
            System.out.println("Organizador");

            String menu = sc.nextLine().toLowerCase();
            if (menu.equals("actividades")) {
                System.out.print("Escoje la categoria\nPuedes escoger entre:\n1: Exposicio\n2: Concurs\n3: Infantil\n4: Trobada\n");
                String ac[]={"Exposcio","Concurs","Infantil","Trobada"};
                int actividades=sc.nextInt();

                for (int i = 0; i < matriu[0].length-1; i++) {
                    if (ac[actividades-1].equals(matriu[i][6])) {
                        for (int j = 0; j < matriu.length; j++) {
                            System.out.print(matriu[i][j] + " ");
                        }
                        System.out.println();
                    }
                }
            }
            if (menu.equals("fechas")) {
                System.out.print("Escoje el dia\nPuedes escoger entre:\n1: Dilluns\n2: Dimarts\n3: Dimecres\n4: Dijous\n");
                String dies[] = {"Dilluns", "Dimarts", "Dimecres", "Dijous"};
                int dia = sc.nextInt();
                for (int i = 0; i < matriu[0].length-1; i++) {
                    if (dies[dia-1].equals(matriu[i][0])){
                        for (int j=0;j<matriu.length;j++){
                            System.out.print(matriu[i][j]+" ");
                        }
                        System.out.println();
                    }
                }
            }

            if (menu.equals("organizador")) {
                System.out.print("Esoje el organizador\nPuedes escoger entre:\n1: Casal Fent Gran Taulat - Can Saladrigas\n2: Comissió de Festes Diagonal-Montseny\n3: Eixos Creativa\n4: Ateneu Popular la Flor de Maig\n");
                int or=sc.nextInt();
                String organo[]={"Casal Fent Gran Taulat - Can Saladrigas","Comissió de Festes Diagonal-Montseny","Eixos Creativa","Ateneu Popular la Flor de Maig"};
                for (int i = 0; i < matriu[0].length-1; i++) {
                    if (organo[or-1].equals(matriu[i][4])) {
                        for (int j = 0; j < matriu.length; j++) {
                            System.out.print(matriu[i][j] + " ");
                        }
                        System.out.println();
                    }
                }
            }

            if (menu.equals("localizacion")) {
                System.out.print("Escoje el lugar\nPuedes escoger entre:\n1: Casal de Gent Gran Taulat Can Saladrigas, c. Llull 214-216\n2: a la pl. Montseny (av. Diagonal 62-82)\n3: a l'Espai Jove 'Roc Boronat', c. Roc Boronat amb c. Pallars\n4: a l'Ateneu Popular La Flor de Maig, c. Doctor Trueta, 195\n");
                int loca=sc.nextInt();
                String local[]={"Casal de Gent Gran Taulat Can Saladrigas, c. Llull 214-216","a la pl. Montseny (av. Diagonal 62-82)","a l'Espai Jove 'Roc Boronat', c. Roc Boronat amb c. Pallars","a l'Ateneu Popular La Flor de Maig, c. Doctor Trueta, 195"};
                for (int i = 0; i < matriu[0].length-1; i++) {
                    if (local[loca-1].equals(matriu[i][5])) {
                        for (int j = 0; j < matriu.length; j++) {
                            System.out.print(matriu[i][j] + " ");
                        }
                        System.out.println();
                    }
                }
            }
            if (menu.equals("horario general")){
                for (int i =1;i<matriu.length;i++){
                    for (int j =0;j<matriu[0].length;j++){
                        System.out.print(matriu[i][j]+" ");
                    }
                    System.out.println(" ");
                }
            }
        System.out.println("adios");
        }
    }